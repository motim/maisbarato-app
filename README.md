## Steps to get up and running

First, install [Nodejs](https://nodejs.org/en/download/). Npm is comming together. 

Second, install Ionic globally:

```bash
$ npm install -g ionic cordova
```

Third, install project's dependencies:
```bash
$ npm install
```

Fourth, create a `environment.dev.ts` inside environments folder.

After that, you can start the project like this:
```bash
$ ionic serve
```

To deploy the app in both platforms you need to add them first:

```bash
$ ionic cordova platform add ios
$ ionic cordova run ios
```

I think this is it. Oh, a little tip: some changes demands to restart `ionic serve`.

Cheers. 
 
