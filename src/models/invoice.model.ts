import { Issuer, parseRawIssuer } from "./issuer.model";
import { InvoiceItem, parseRawProduct } from "./product.model";

export interface Invoice {
  modelo: number;
  serie: number;
  numero: number;
  data: Date;
  valor: number;
  chaveAcesso: string;
  emitente: Issuer;
  produtos: InvoiceItem[],
  url: string
}

export function parseRawInvoice(rawData): Invoice{
  return {
    modelo: Number.parseInt(rawData.modelo),
    serie: Number.parseInt(rawData.serie),
    numero: Number.parseInt(rawData.numero),
    data: parseDate(rawData.data, rawData.hora),
    valor: parseValor(rawData.valor),
    chaveAcesso: parseChaveAcesso(rawData.chave_acesso),
    emitente: parseIssuer(rawData.emitente),
    produtos: parseProducts(rawData.produtos),
    url: rawData.url
  }
}

export function parseDate(rawDate: string, rawHour: string): Date{
  let [day, month, year] = rawDate.split('/');
  return new Date(`${year}-${month}-${day}T${rawHour}`);
}

export function parseValor(rawValor){
  return Number.parseFloat(rawValor.replace(/\./g,'').replace(/\,/g,'.'));
}

export function parseChaveAcesso(rawKey): string{
  return rawKey.replace(/\s/g,'');
}

export function parseIssuer(rawIssuer): Issuer{
  return parseRawIssuer(rawIssuer);
}

export function parseProducts(rawProducts: any[]): InvoiceItem[]{
  let products = [];
  let productsTable = {};
  rawProducts.forEach(rawProduct => {
    // console.log('prod: ', rawProduct.descricao, rawProduct.codigo);
    // console.log('parseValor(rawProduct.qtd): ', parseValor(rawProduct.qtd));
    const rawProductQtd = parseValor(rawProduct.qtd);
    const rawProductTotalValue = parseValor(rawProduct.valor);
    if (productsTable.hasOwnProperty(rawProduct.codigo)){
      // console.log('updating...');
      productsTable[rawProduct.codigo]['count'] = productsTable[rawProduct.codigo]['count'] + 1;
      productsTable[rawProduct.codigo]['qtd'] = productsTable[rawProduct.codigo]['qtd'] + rawProductQtd;
      productsTable[rawProduct.codigo]['valor'] = productsTable[rawProduct.codigo]['valor'] + rawProductTotalValue;
    }else {
      // console.log('creating...');
      productsTable[rawProduct.codigo] = {
        'item': parseRawProduct(rawProduct),
        'count': 1,
        'qtd': rawProductQtd,
        'valor': rawProductTotalValue
      }
    }
    // console.log('count: ', productsTable[rawProduct.codigo]['count']);
    // console.log('qtd: ', productsTable[rawProduct.codigo]['qtd']);
    // console.log('valor: ', productsTable[rawProduct.codigo]['valor']);
    // console.log('----------------------------')

  });

  for (const key in productsTable) {
    productsTable[key]['item']['count'] = productsTable[key]['count']
    productsTable[key]['item']['qtd'] = productsTable[key]['qtd']
    productsTable[key]['item']['valor'] = productsTable[key]['valor']
    products.push(productsTable[key]['item'])
  }
  return products;
}

