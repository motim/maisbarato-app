import { parseValor } from "./invoice.model";

export interface InvoiceItem{
  descricao: string;
  qtd: number;
  unidadeComercial: string;
  valor: number;
  valorUnitario: number;
  codigo: string;
  ncm: string;
  cfop: number;
  eanComercial: string;
  eanTributavel: string;
  count: number,
  desconto: number
 } // count how many times the same item appears into invoice

export function parseRawProduct(rawProduct): InvoiceItem {
  return {
    descricao: rawProduct.descricao,
    unidadeComercial: rawProduct.unidade_comercial,
    codigo: rawProduct.codigo,
    ncm: rawProduct.ncm,
    eanComercial: rawProduct.ean_comercial,
    eanTributavel: rawProduct.ean_tributavel,
    cfop: Number.parseInt(rawProduct.cfop),
    valor: parseValor(rawProduct.valor),
    valorUnitario: parseValor(rawProduct.valor_unitario),
    qtd: parseValor(rawProduct.qtd),
    count: 1,
    desconto: parseValor(rawProduct.desconto)
  }
}
