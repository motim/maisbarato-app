import {Invoice, parseRawInvoice} from './invoice.model';

test('parseProducts', () => {

  const rawInvoice = {
    "modelo": "65",
    "serie": "329",
    "numero": "45390",
    "data": "26\/08\/2017",
    "hora": "17:54:56",
    "valor": "318,84",
    "chave_acesso": "2917 0847 5084 1108 9203 6532 9000 0453 9010 0003 8336",
    "emitente": {
      "cnpj": "47.508.411\/0892-03",
      "razao_social": "CIA BRASILEIRA DE DISTRIBUICAO",
      "ie": "042601557",
      "uf": "BA",
      "municipio": {
        "nome": "SALVADOR",
        "numero": "2927408"
      },
      "bairro": "PARALELA",
      "endereco": "AV. GOV. LUIS VIANA FILHO,  3056",
      "cep": "41820-725",
      "nome_fantasia": "EXH - PARALELA"
    },
    "produtos": [
      {
        "descricao": "NINHO F1 MAIS SACHE",
        "qtd": "4,0000",
        "unidade_comercial": "UN",
        "valor": "115,60",
        "valor_unitario": "28,9000",
        "codigo": "5169832",
        "ncm": "19011010",
        "cfop": "5102",
        "ean_comercial": "07891000090480",
        "ean_tributavel": "07891000090480"
      },
      {
        "descricao": "FDA HUG SUPREME CR H",
        "qtd": "3,0000",
        "unidade_comercial": "UN",
        "valor": "212,70",
        "valor_unitario": "70,9000",
        "codigo": "1124041",
        "ncm": "96190000",
        "cfop": "5405",
        "ean_comercial": "07896007548484",
        "ean_tributavel": "07896007548484"
      },
      {
        "descricao": "ESC DENTAL BAMBINOS",
        "qtd": "1,0000",
        "unidade_comercial": "UN",
        "valor": "7,90",
        "valor_unitario": "7,9000",
        "codigo": "1113113",
        "ncm": "96032100",
        "cfop": "5405",
        "ean_comercial": "07891055430637",
        "ean_tributavel": "07891055430637"
      }
    ]
  }

  const newInvoice = parseRawInvoice(rawInvoice);
  expect(newInvoice.produtos.length).toBe(3);
});
