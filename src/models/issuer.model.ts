export interface City {nome: string, numero: string}
export interface Issuer {
  cnpj: string
  razao_social: string
  ie: string
  uf: string
  municipio: City
  bairro: string
  endereco: string
  cep: string
  nome_fantasia: string

}

export function parseRawIssuer(rawIssuer): Issuer {
  return {
    cnpj: parseCnpj(rawIssuer.cnpj),
    razao_social: rawIssuer.razao_social,
    ie: rawIssuer.ie,
    uf: rawIssuer.uf,
    municipio: rawIssuer.municipio,
    bairro: rawIssuer.bairro,
    endereco: rawIssuer.endereco,
    cep: rawIssuer.cep,
    nome_fantasia: rawIssuer.nome_fantasia
  }
}

export function parseCnpj(rawCnpj){
  return rawCnpj.replace(/\./g,'').replace(/\-/g,'').replace(/\//g,'');
}

