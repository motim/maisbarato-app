import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InvoiceDetailPage } from './invoice-detail';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    InvoiceDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(InvoiceDetailPage),
    PipesModule
  ],
})
export class InvoiceDetailPageModule {}
