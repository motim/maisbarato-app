import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Invoice } from "../../models/invoice.model";

/**
 * Generated class for the InvoiceDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-invoice-detail',
  templateUrl: 'invoice-detail.html',
})
export class InvoiceDetailPage {

  public invoice: Invoice;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.invoice = this.navParams.get('invoice');
  }

  getPlaceName(){
    return this.invoice.emitente.nome_fantasia || this.invoice.emitente.razao_social
  }

}
