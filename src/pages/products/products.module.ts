import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductsPage } from './products';
import { PipesModule } from '../../pipes/pipes.module';
import { InstantSearchProvider } from '../../providers/instant-search/instant-search';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ProductsPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductsPage),
    PipesModule,
    ComponentsModule
  ],
  providers: [
    InstantSearchProvider
  ]
})
export class ProductsPageModule {}
