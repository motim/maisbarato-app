import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { InstantSearchProvider } from '../../providers/instant-search/instant-search';
import { connectHits, connectInfiniteHits, connectSearchBox } from 'instantsearch.js/es/connectors';
import { OnInit, AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';


@IonicPage()
@Component({
  selector: 'page-products',
  templateUrl: 'products.html'
})
export class ProductsPage implements OnInit, AfterViewInit {
  // products: Observable<any[]>;
  // private productsRef: AngularFirestoreCollection<any>;
  state: {
    hits: { descricao: string; precoMedio: number }[];
    isLastPage: boolean;
    showMore: Function;
  } = { hits: [], isLastPage: false, showMore: undefined };

  stateSearch: { query: string; refine: Function } = {
    query: '',
    refine() {}
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public db: AngularFirestore,
    private instantSearchService: InstantSearchProvider
  ) {
    // this.productsRef = this.db.collection<any>('products')
    // this.products = this.productsRef.valueChanges();
    // this.products = this.db.collection('products', ref => ref.orderBy('precoMedio')).valueChanges();
  }

  ngOnInit() {
    const widget = connectInfiniteHits(this.updateState);
    this.instantSearchService.search.addWidget(widget());

    const widgetSearch = connectSearchBox(this.updateStateSearch);
    this.instantSearchService.search.addWidget(widgetSearch());
  }

  ngAfterViewInit() {
    const isStarted = Object.keys(this.instantSearchService.search).indexOf('started') !== -1;
    if (!isStarted){
      this.instantSearchService.search.start();
    }
  }

  updateState = (state, isFirstRendering) => {
    // asynchronous update of the state
    // avoid `ExpressionChangedAfterItHasBeenCheckedError`
    // console.log('updateState: ', state);
    if (isFirstRendering) {
      return Promise.resolve().then(() => {
        this.state = state;
      });
    }

    this.state = state;
  };

  updateStateSearch = (state, isFirstRendering) => {
    // asynchronous update of the state
    // avoid `ExpressionChangedAfterItHasBeenCheckedError`
    // console.log('updateStateSearch: ', state);
    if (isFirstRendering) {
      return Promise.resolve().then(() => {
        this.stateSearch = state;
      });
    }

    this.stateSearch = state;
  };

  searchProducts(searchTerm: string) {
    // console.log('searchProducts: ', this.stateSearch);
    this.stateSearch.refine(searchTerm);
  }

  openDetails(product) {
    this.navCtrl.push('ProductDetailsPage', { product: product });
  }

  doInfinite() {
    return Promise.resolve().then(() => {
      this.state.showMore();
    });
  }

  isNotLastPage() {
    return !this.state['isLastPage'];
  }
}
