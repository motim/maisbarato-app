import { Component } from '@angular/core';
import { NavController, AlertController } from "ionic-angular";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { NfceCrawlerProvider } from "../../providers/nfce-crawler/nfce-crawler";
import { LoadingController, Platform } from "ionic-angular";
import { Invoice, parseRawInvoice } from "../../models/invoice.model";
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  invoices: Observable<any[]>;
  private invoicesRef: AngularFirestoreCollection<any>;
  private nonProcessedInvoicesRef: AngularFirestoreCollection<any>;

  constructor(
    public navCtrl: NavController,
    private barcodeScanner: BarcodeScanner,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public nfceCrawlerProvider: NfceCrawlerProvider,
    public db: AngularFirestore,
    public platform: Platform
  ) {
    this.invoicesRef = this.db.collection<any>('invoices');
    this.nonProcessedInvoicesRef = this.db.collection<any>('non-processed-invoices');
    this.invoices = this.invoicesRef.valueChanges();
  }

  scan() {
    this.barcodeScanner.scan().then(
      barcodeData => {
        if (barcodeData.format.length > 0 && barcodeData.format !== 'QR_CODE') {
          this.showWrongFormatAlert();
        } else if (
          barcodeData.format.length > 0 &&
          barcodeData.format === 'QR_CODE'
        ) {
          // loading
          try {
            const chaveAcesso = this.getChaveAcesso(barcodeData.text);
            const waitLoading = this.wait();
            this.invoiceExists(chaveAcesso).then(snapshot => {
              if (
                snapshot === null ||
                snapshot === undefined ||
                snapshot.empty
              ) {
                this.nfceCrawlerProvider
                  .getNfceData(barcodeData.text)
                  .subscribe(
                    data => {
                      this.done(waitLoading);
                      this.invoicesRef.add(parseRawInvoice(data));
                    },
                    err => {
                      if (err.error.code === 'NOT_FOUND_BUTTON'){
                        this.nonProcessedInvoicesRef.doc(chaveAcesso).set({url: barcodeData.text});
                      }
                      this.showHttpRequestError(err);
                      this.done(waitLoading);
                    }
                  );
              } else {
                this.showDuplicatedInvoiceAlert();
                this.done(waitLoading);
              }
            });
          } catch (error) {
            this.showWrongURLFormatAlert();
            // this.done(waitLoading);
          }
        }
      },
      err => {
        console.log('error reading QR Code: ', err);
      }
    );
  }

  showWrongFormatAlert() {
    this.alertCtrl
      .create({
        title: 'Formato incorreto.',
        subTitle: 'O único formato permitido é o QR Code.',
        buttons: ['OK']
      })
      .present();
  }

  showWrongURLFormatAlert() {
    this.alertCtrl
      .create({
        title: 'Formato incorreto.',
        subTitle: 'Parece que o QR Code está mal formatado. =( ',
        buttons: ['OK']
      })
      .present();
  }

  showDuplicatedInvoiceAlert() {
    this.alertCtrl
      .create({
        title: 'Cupom duplicado',
        subTitle: 'Esse cupom já consta na nossa base de dados.',
        buttons: ['OK']
      })
      .present();
  }

  showHttpRequestError(err) {
    this.alertCtrl
      .create({
        title: 'Erro no servidor',
        subTitle: err.error.message || 'Serviço indisponível',
        buttons: ['OK']
      })
      .present();
  }

  wait() {
    const waitLoading = this.loadingCtrl.create({
      content: 'Extraindo dados...'
    });

    waitLoading.present();

    return waitLoading;
  }

  done(loadingRef) {
    loadingRef.dismiss();
  }

  getChaveAcesso(url: string) {
    return url
      .split('&')[0]
      .split('?')[1]
      .split('=')[1]
      .replace(/\s/g, ''); // chNFe value
  }

  invoiceExists(chaveAcesso: string) {
    return this.invoicesRef.ref.where('chaveAcesso', '==', chaveAcesso).get();
  }

  openDetail(invoice: Invoice) {
    this.navCtrl.push('InvoiceDetailPage', {
      invoice: invoice
    });
  }

  isCordova() {
    return this.platform.is('cordova');
  }
}
