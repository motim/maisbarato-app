import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManualExtractionPage } from './manual-extraction';

@NgModule({
  declarations: [
    ManualExtractionPage,
  ],
  imports: [
    IonicPageModule.forChild(ManualExtractionPage),
  ],
})
export class ManualExtractionPageModule {}
