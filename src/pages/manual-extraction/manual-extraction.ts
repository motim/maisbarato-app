import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { ParsersProvider } from '../../providers/parsers/parsers';
import { parseRawInvoice } from '../../models/invoice.model';


@IonicPage()
@Component({
  selector: 'page-manual-extraction',
  templateUrl: 'manual-extraction.html',
  providers: [ParsersProvider]
})
export class ManualExtractionPage {

  public isParsingProducts = false;
  public isParsingProductsDone = false;
  public isParsingNfe = false;
  public isParsingNfeDone = false;
  public isParsingIssuer = false;
  public isParsingIssuerDone = false;
  public dbIssuerList;
  public dbItensList;
  public parsedNfce: any;
  public parsedIssuer: any;
  public parsedItens: any;

  public invoices: Observable<any[]>;
  private invoicesRef: AngularFirestoreCollection<any>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public parsers: ParsersProvider,
    public db: AngularFirestore) {

      this.invoicesRef = this.db.collection<any>('invoices')
      this.invoices = this.invoicesRef.valueChanges();
  }

  parseProducts(event) {
    this.isParsingProducts = true;
    this.isParsingProductsDone = false;
    if (event.target.files !== undefined && event.target.files[0] !== undefined) {
      let reader = new FileReader();

      reader.onload = (e: any) => {
        const htmlContent: string = e.target.result;
        const isValidProductsHtml = htmlContent.indexOf('CFOP') !== -1;
        if (isValidProductsHtml){
          this.parsers.parseProducts(e.target.result).subscribe(res => {
            console.log(res);
            this.isParsingProducts = false;
            this.isParsingProductsDone = true;
            this.parsedItens = res
          });
        }else {
          this.wrongHTMLAlert('Este não é o HTML de Produtos e Serviços.');
          this.isParsingProducts = false;
        }
      }

      reader.readAsText(event.target.files[0]);
    }
  }

  parseNfce(event) {
    this.isParsingNfe = true;
    this.isParsingNfeDone = false;
    if (event.target.files !== undefined && event.target.files[0] !== undefined) {
      let reader = new FileReader();

      reader.onload = (e: any) => {
        const htmlContent: string = e.target.result;
        const validHtml = htmlContent.indexOf('Dados da NFC-e') !== -1;
        if (validHtml){
          this.parsers.parseNfce(e.target.result).subscribe(res => {
            this.isParsingNfe = false;
            this.isParsingNfeDone = true;
            this.parsedNfce = res;
            console.log(this.parsedNfce);
          });
        }else {
          this.wrongHTMLAlert('Este não é o HTML da aba Nfe.');
          this.isParsingNfe = false;
        }
      }

      reader.readAsText(event.target.files[0]);
    }
  }

  parseIssuer(event) {
    this.isParsingIssuer = false;
    this.isParsingIssuerDone = false;

    if (event.target.files !== undefined && event.target.files[0] !== undefined) {
      let reader = new FileReader();

      reader.onload = (e: any) => {
        const htmlContent: string = e.target.result;
        const validHtml = htmlContent.indexOf('Dados do Emitente') !== -1;
        if (validHtml){
          this.parsers.parseIssuer(e.target.result).subscribe(res => {
            this.isParsingIssuer = false;
            this.isParsingIssuerDone = true;
            this.parsedIssuer = res;
            console.log(this.parsedIssuer);
          });
        }else {
          this.wrongHTMLAlert('Este não é o HTML da aba de Emitente.');
          this.isParsingIssuer = false;
        }
      }

      reader.readAsText(event.target.files[0]);
    }
  }

  wrongHTMLAlert(subTitle) {
    let alert = this.alertCtrl.create({
      title: 'HTML incorreto',
      subTitle: subTitle,
      buttons: ['Ok']
    });
    alert.present();
  }

  sendInvoice(){
    this.parsedNfce['produtos'] = this.parsedItens;
    this.parsedNfce['emitente'] = this.parsedIssuer;
    const invoice = parseRawInvoice(this.parsedNfce);
    this.db.collection('invoices', ref => ref.where('chaveAcesso', '==', invoice.chaveAcesso)).valueChanges()
      .subscribe(result => {
        if (!result){
          this.invoicesRef.add(invoice);
        }else {
          this.wrongHTMLAlert('Esse cupom já foi enviado.');
        }
      });
  }

}
