import { Injectable } from '@angular/core';
import instantsearch from 'instantsearch.js/es';

@Injectable()
export class InstantSearchProvider {

  search = instantsearch({
    appId: 'KV9H2DMT1G',
    apiKey: '0a35897bd9709a08aa444a1287ac92d8',
    indexName: 'products',
    urlSync: true
  });

  constructor() {}
}
