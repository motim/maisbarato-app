import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ENV } from '@app/env'

@Injectable()
export class ParsersProvider {

  public parseProductsEndpoint = `${ENV.nfceEndpoint}/products`;
  public parseNfceEndpoint = `${ENV.nfceEndpoint}/cupons`;
  public parseIssuerEndpoint = `${ENV.nfceEndpoint}/issuers`;

  constructor(public http: HttpClient) {}

  parseProducts(html_string) {
    return this.parse(this.parseProductsEndpoint, html_string);
  }

  parseNfce(html_string) {
    return this.parse(this.parseNfceEndpoint, html_string);
  }

  parseIssuer(html_string) {
    return this.parse(this.parseIssuerEndpoint, html_string);
  }

  private parse(endpoint, html_string) {
    const body = JSON.stringify({ 'html_string': html_string  });
    return this.http.post(endpoint, body);
  }

}
