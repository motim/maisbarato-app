import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from "rxjs/Observable";
import { Invoice, parseRawInvoice } from "../../models/invoice.model";
import { HttpClient } from '@angular/common/http';
import { ENV } from '@app/env'

@Injectable()
export class NfceCrawlerProvider {

  endpoint = `${ENV.nfceEndpoint}/nfce?url=`;

  constructor(public http: HttpClient) {}

  getNfceData(url: string): Observable<Invoice> {
    return this.http.get<Invoice>(`${this.endpoint}${encodeURIComponent(url)}`);
  }

  parseInvoice(rjson){
    return parseRawInvoice(rjson);
  }

}
