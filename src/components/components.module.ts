import { NgModule } from '@angular/core';
import { ProductComponent } from './product/product';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
	declarations: [ProductComponent],
	imports: [IonicModule, PipesModule],
	exports: [ProductComponent]
})
export class ComponentsModule {}
