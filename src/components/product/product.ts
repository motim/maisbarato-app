import { Component, Input } from '@angular/core';

@Component({
  selector: 'product',
  templateUrl: 'product.html'
})
export class ProductComponent {

  text: string;
  @Input() product;

  constructor() {
    this.text = 'Hello World';
  }

}
