export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyAj1FHBJqrzGScOtaSMcAGf2huYCcVOjJE",
    authDomain: "supermarkets-products-index.firebaseapp.com",
    databaseURL: "https://supermarkets-products-index.firebaseio.com",
    projectId: "supermarkets-products-index",
    storageBucket: "supermarkets-products-index.appspot.com",
    messagingSenderId: "964180158495"
  }
};

export const ENV = {
  mode: 'prod',
  nfceEndpoint: 'https://motim-nfce-crawler.now.sh'
}
