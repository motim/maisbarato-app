# 0.1 (01/10/2017)

‐ Leitura da nota fiscal por QRCode

‐ Envio da URL do QRCode para o crawler

‐ Preparação dos dados crus do crawler para tipos de dados com melhor manipulação 

‐ Adição dos dados extraídos no Db

‐ Lista das notas extraídas

‐ Página com detalhamento de cada nota

‐ Previne o cadastro de notas repetidas

-Aglutinar os itens da nota para diminuir a quantidade de itens no banco

-Tratar erro do servidor de crawler (503, 403, etc.)

# 0.2 (27/01/2018)

Bug fixes:

 - Nota fiscal trava o app no loading
 - Ao abrir o leitor de qrcode e não ler código nenhum, voltando pra tela anterior da mensagem de código inválido pra qrcode.

Features:

 - Busca de produtos 
